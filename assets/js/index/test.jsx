import React from 'react'

class SubTasks extends React.Component {
  constructor(props) {
    super(props);
    this.subTasks = props.subTasks;
    this.incrementStatus = this.incrementStatus.bind(this);
    this.updateSubTasks = props.updateSubTasks;
    this.taskId = props.taskId;
  }

  incrementStatus(title) {
    let subTasks = [...this.subTasks];

    for (let i = 0; i < subTasks.length; i++) {
      if (title === subTasks[i].title) {
        subTasks[i].status++;
      }
    }
    this.updateSubTasks(this.taskId, subTasks);
  }

  render() {
    const subTaskItems = this.subTasks.map(subTask => <li key={subTask.title}>
      <button onClick={() => {this.incrementStatus(subTask.title)}}>incrementStatus</button>
      {subTask.title}: {subTask.status}
    </li>);

    return <ul>
      {subTaskItems}
    </ul>
  }
}

class Task extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tasks: [
        {
          title: "task 1",
          subTasks: [
            { status: 0, title: "subTask 1" },
            { status: 0, title: "subTask 2" },
            { status: 0, title: "subTask 3" },
          ]
        },
        {
          title: "task 2",
          subTasks: [
            { status: 0, title: "subTask 4" },
            { status: 0, title: "subTask 5" },
            { status: 0, title: "subTask 6" },
          ]
        }
      ]
    };
    this.old_tasks = [
      {
        title: "task 1",
        subTasks: [
          { status: 0, title: "subTask 1" },
          { status: 0, title: "subTask 2" },
          { status: 0, title: "subTask 3" },
        ]
      },
      {
        title: "task 2",
        subTasks: [
          { status: 0, title: "subTask 4" },
          { status: 0, title: "subTask 5" },
          { status: 0, title: "subTask 6" },
        ]
      }
    ];
    this.replaceWithOtherTasks = this.replaceWithOtherTasks.bind(this);
    this.updateSubTasks = this.updateSubTasks.bind(this);
  }

  replaceWithOtherTasks() {
    console.log("replaceWithOtherTasks");
    if (this.state.tasks.length !== 0)
    {
      console.log("replaceWithOtherTasks =0");
      // let otherTasks = [...this.state.tasks];
      // otherTasks[0].subTasks[0].status = 100;
      this.setState({tasks: []});
    }
    else
    {
      console.log("replaceWithOtherTasks >0");
      this.setState({tasks: [...this.old_tasks]});
    }
  }

  updateSubTasks(taskId, subTasks) {
    let updatedTasks = [...this.state.tasks];

    for (let i = 0; i < updatedTasks.length; i++) {
      if (taskId === updatedTasks[i].title) {
        updatedTasks[i].subTasks = [...subTasks];
      }
    }

    this.setState({tasks: updatedTasks})
  }

  render () {
    const taskItems = this.state.tasks.map(task =>
      (<li key={task.title}>
        <h2>{task.title}</h2>
        <SubTasks taskId={task.title} updateSubTasks={this.updateSubTasks} subTasks={task.subTasks}/>
      </li>));

    return (
      <div>
        <div>
          <button onClick={this.replaceWithOtherTasks}>replaceWithOtherTasks</button>
          <ul>
            {taskItems}
          </ul>
        </div>
      </div>
    );
  }
}

export default Task;
