import React from 'react'
import {get as getCookie} from "browser-cookies/src/browser-cookies";
import axios from "axios/index";


const updateSubTask = (subTask, callback) => {
  const mutation = `mutation {
    updateSubTask(id: ${subTask.id}, shortStatus: "${subTask.status}") {
      ok
      subTask {
        id
        title
        timeStart
        spentMinutes
        loggedMinutes
        status
      }
    }
  }`;

  axios({
    url: '/graphql',
    method: 'post',
    data: {
      query: mutation
    },
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': getCookie('csrftoken'),
    }
  }).then((result) => {
    let updateSubTask = result.data.data.updateSubTask;
    if (updateSubTask.ok) {
      callback(updateSubTask.subTask);
    }
  });
};

export default updateSubTask
