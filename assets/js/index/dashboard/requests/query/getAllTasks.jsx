import React from 'react'
import axios from "axios/index";


const getAllTasks = (callback) => {
  axios({
    url: '/graphql',
    method: 'get',
    params: {
      query: `query {
        allTasks {
          id
          title
          description
          group
          status
          statusReason
          pullRequests
          taskLink
          logTaskId
          
          subtaskSet {
            id
            title
            timeStart
            spentMinutes
            loggedMinutes
            status
          }
        }
      }`
    },
  }).then((result) => {
    callback(result.data.data.allTasks);
  });
};

export default getAllTasks
