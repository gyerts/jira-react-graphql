import React from 'react';

import { Card, Button, CardHeader, CardFooter, CardBody,
  CardTitle, CardText } from 'reactstrap';

// import SubTasks from './subtasks/subtasks'
import SubTask from './subtasks/subtask/subtask'


class task extends React.Component {
  constructor(props) {
    super(props);
    this.task = props.task;
    this.domUpdateTask = props.domUpdateTask;
    this.domUpdateSubTask = this.domUpdateSubTask.bind(this);
    this.handleUpdateSubTaskStatus = this.handleUpdateSubTaskStatus.bind(this);
  }

  domUpdateSubTask(subTask) {
    let updatedTask = {...this.task};
    let subTasks = [...updatedTask.subtaskSet];

    for (let i = 0; i < subTasks.length; i++) {
      if (subTasks[i].id === subTask.id) {
        updatedTask.subtaskSet[i] = subTask;
        break;
      }
    }
    this.domUpdateTask(updatedTask);
  }

  handleUpdateSubTaskStatus(id, status) {
    const subTasks = this.task.subtaskSet;
    for (let i = 0; i < subTasks.length; i++) {
      if (subTasks[i].id === id) {
        let foundSubTask = {...subTasks[i]};
        foundSubTask.status = status;
        this.domUpdateSubTask( foundSubTask );
        break;
      }
    }
  }


  render() {
    const subTaskItems = this.task.subtaskSet.map(subTask =>
      <SubTask
        key={subTask.id}
        id={subTask.id}
        handleUpdateSubTaskStatus={this.handleUpdateSubTaskStatus}
        subTask={subTask} />
    );

    return (
      <Card style={{marginBottom: "5px", marginTop: "5px"}}>
        <CardHeader>{this.task.title}</CardHeader>
        <CardBody>
          <CardTitle>{this.task.statusReason}</CardTitle>
          <CardText>{this.task.description}</CardText>
        </CardBody>
        <CardFooter>

          <table width="100%">
            <tbody>
            {subTaskItems}
            </tbody>
          </table>

        </CardFooter>
      </Card>);
  }
}

export default task;
