import React from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, UncontrolledDropdown, DropdownItem } from 'reactstrap';

const statuses = {
  "PND": {
    name: "Pending", classToAdd: "badge-secondary"
  },
  "REJ": {
    name: "Rejected", classToAdd: "badge-danger"
  },
  "DEP": {
    name: "Deprecated", classToAdd: "badge-warning"
  },
  "DIF": {
    name: "Differed", classToAdd: "badge-secondary"
  },
  "BLC": {
    name: "Blocked", classToAdd: "badge-danger"
  },
  "IPR": {
    name: "In progress", classToAdd: "badge-info"
  },
  "DNE": {
    name: "Done", classToAdd: "badge-success"
  },
};

const getDropDownItem = (callback, id, status) => {
  const statusObj = statuses[status];
  return <DropdownItem
    key={status}
    style={{padding: "0", cursor: "pointer"}}
    onClick={() => callback(id, status)}>
    <a className="dropdown-item" href="/subtask/49/change_status/IPR">
      <span className={`badge badge-pill my_budge ${statusObj.classToAdd}`}>{statusObj.name}</span>
    </a>
  </DropdownItem>;
};

class DropDownSettings {
  constructor() {
    this.dropDownOpen = false;
  }
  toggle (status) {
    this.dropDownOpen = !this.dropDownOpen;
  }
}


const FDropDownStatus = (props) => {
  let dropDownSettings = new DropDownSettings;

  let variants = [];
  variants.push( getDropDownItem(props.handleUpdateSubTaskStatus, props.id, "PND") );
  variants.push( getDropDownItem(props.handleUpdateSubTaskStatus, props.id, "REJ") );
  variants.push( getDropDownItem(props.handleUpdateSubTaskStatus, props.id, "DEP") );
  variants.push( getDropDownItem(props.handleUpdateSubTaskStatus, props.id, "DIF") );
  variants.push( getDropDownItem(props.handleUpdateSubTaskStatus, props.id, "BLC") );
  variants.push( getDropDownItem(props.handleUpdateSubTaskStatus, props.id, "IPR") );
  variants.push( getDropDownItem(props.handleUpdateSubTaskStatus, props.id, "DNE") );

  return (
    <UncontrolledDropdown>
      <DropdownToggle
        caret
        className={`badge badge-pill my_budge ${statuses[props.status].classToAdd}`}
        style={{
          float: "left",
          width: "100px",
          marginRight: "5px",
          marginTop: "3px"
        }}>{statuses[props.status].name}</DropdownToggle>

      <DropdownMenu>
        {variants}
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};


export default FDropDownStatus;
