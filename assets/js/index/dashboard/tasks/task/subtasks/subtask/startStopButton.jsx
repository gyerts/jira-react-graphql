import React from 'react';

const startStopButton = (props) => {
  return (
    <a href="/subtask/49/start">
      <img src="/static/img/play.jpeg" alt="" width="30px" />
    </a>
  );
};

export default startStopButton;
