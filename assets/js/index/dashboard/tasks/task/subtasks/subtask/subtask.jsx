import React from 'react';

import {
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupDropdown,
  Input,
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

import './style.css'
import DropDownStatus  from './dropDownStatus'
import Title           from './title'
import SpentTime       from './spentTime'
import LoggedTime      from './loggedTime'
import LogTime         from './logTime'
import StartStopButton from './startStopButton'


const subTask = (props) => {
  return (
    <tr style={{border: "solid 1px #e2e2e2"}}>
      <td>
        <DropDownStatus
          id={props.id}
          handleUpdateSubTaskStatus={props.handleUpdateSubTaskStatus}
          status={props.subTask.status}/>

        <Title title={props.subTask.title} />
      </td>

      <td width="130px">
        <LoggedTime />
      </td>
      <td width="130px">
        <SpentTime />
      </td>

      <td style={{textAlign: "center", width: "30px"}}>
        <LogTime />
      </td>
      <td style={{textAlign: "center", width: "30px"}}>
        <StartStopButton />
      </td>
    </tr>
  );
}

export default subTask;
