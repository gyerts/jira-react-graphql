import React from 'react';

const spentTime = (props) => {
  return (
    <div id="subtask_49_spent" style={{
      marginBottom: "3px",
      textAlign: "center",
      backgroundColor: "rgb(233, 234, 233)",
      color: "#5d5844",
      width: "100%"
    }}>7h 30m</div>
  );
};

export default spentTime;
