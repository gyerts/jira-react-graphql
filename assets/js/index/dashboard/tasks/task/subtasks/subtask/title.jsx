import React from 'react';

const Title = (props) => {
  return (
    <a href="/admin/tasks/subtask/49/change/" style={{textDecoration: "none"}}>
      {props.title}
    </a>
  );
};

export default Title;
