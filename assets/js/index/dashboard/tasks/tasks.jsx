import React from 'react'
import Task from './task/task'

import getAllTasks from "../requests/query/getAllTasks"


class TaskList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
    };
    this.domApplyTasks = this.domApplyTasks.bind(this);
    this.domUpdateTask = this.domUpdateTask.bind(this);
  }

  componentDidMount() {
    getAllTasks(this.domApplyTasks);
  }

  domApplyTasks(tasks) {
    this.setState({
      tasks: tasks,
    });
  };

  domUpdateTask (updatedTask) {
    let tasks = [...this.state.tasks];

    for (let i = 0; i < tasks.length; i++) {
      if (tasks[i].id === updatedTask.id) {
        tasks[i] = updatedTask;
        break;
      }
    }
    this.domApplyTasks(tasks);
  }

  testUpdateStatus() {
    let tasks = [...this.state.tasks];
    let subTask = {...tasks[0].subtaskSet[0]};
    subTask.status = "REJ";
    tasks[0].subtaskSet[0] = subTask;
    this.domApplyTasks(tasks);
  }

  render () {
    return (
      <div className="tasks">
        <button onClick={this.testUpdateStatus.bind(this)}>UpdateStatus</button>
        { this.state.tasks.map(task => <Task
                                          domUpdateTask={this.domUpdateTask}
                                          key={task.id}
                                          task={task} />) }
      </div>);
  }
}

export default TaskList
