import React from 'react';
import TaskList from './tasks/tasks'

// https://reactstrap.github.io/components/layout/
import { Container, Row, Col } from 'reactstrap';

class Dashboard extends React.Component {
  render () {
    return <div className="Dashboard">
      <Container fluid={true}>
        <Row>
          <Col md={12}>
            <TaskList />
          </Col>
        </Row>
      </Container>
    </div>
  }
}

export default Dashboard;
