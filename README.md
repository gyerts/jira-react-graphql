Execute once:

    python3 -m virtualenv venv
    source ./venv/bin/activate
    pip install -r requirements.txt
    python manage.py makemigrations impl
    python manage.py migrate
    python manage.py createsuperuser
    
    npm install

For each use:

    python manage.py runserver 127.0.0.1:8000
    npm run watch
