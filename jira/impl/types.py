from graphene_django import DjangoObjectType
import graphene

from .models import Settings
from .models import Task
from .models import SubTask


class SettingsType(DjangoObjectType):
    class Meta:
        model = Settings


class TaskType(DjangoObjectType):
    class Meta:
        model = Task


class SubTaskType(DjangoObjectType):
    class Meta:
        model = SubTask


class CustomType(graphene.ObjectType):
    message = graphene.String()
    task_str = graphene.String()
    task = graphene.Field(TaskType)

    def resolve_task_str(self, info):
        return self.task.__str__()


