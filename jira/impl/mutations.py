from graphene_django import DjangoObjectType
import graphene

from .models import SubTask
from .types import SubTaskType


class UpdateSubTask(graphene.Mutation):
    class Arguments:
        id = graphene.Int()
        short_status = graphene.String()

    ok = graphene.Boolean()
    subTask = graphene.Field(SubTaskType)

    def mutate(self, info, id, short_status):
        if len(short_status) == 3 and SubTask.objects.filter(id=id).exists():
            subTask = SubTask.objects.get(id=id)
            subTask.status = short_status
            subTask.save()
            return UpdateSubTask(subTask=subTask, ok=True)

        return UpdateSubTask(subTask=None, ok=False)


class UpdateMutations(graphene.ObjectType):
    update_sub_task = UpdateSubTask.Field()
