from django.db import models
from django.contrib import admin


class Task(models.Model):
    title = models.CharField(max_length=255, help_text="Имя таски")
    description = models.TextField(help_text="Имя группы", null=True, blank=True)
    group = models.CharField(max_length=255, help_text="группа", default="no group", null=True, blank=True)
    priority = models.IntegerField(default=0, help_text="Приоритет отображенияб чевыше тем больше")

    PENDING = 'PND'
    DIFFERED = 'DIF'
    BLOCKED = 'BLC'
    IN_PROGRESS = 'IPR'
    DONE = 'DNE'

    STATUSES = (
        (PENDING, 'Pending'),
        (DIFFERED, 'Differed'),
        (BLOCKED, 'Blocked'),
        (IN_PROGRESS, 'In progress'),
        (DONE, 'Done'),
    )
    status = models.CharField(
        max_length=3,
        choices=STATUSES,
        default=PENDING,
    )
    status_reason = models.CharField(max_length=255, help_text="причина статуса")

    pull_requests = models.TextField(help_text="Пулл-реквесты", null=True, blank=True)

    def pull_requests_as_list(self):
        ar = []
        if self.pull_requests:
            ar = self.pull_requests.split('\n')
        return ar

    task_link = models.CharField(max_length=1000, help_text="линка на таску", null=True, blank=True)
    log_task_id = models.CharField(max_length=50, help_text="к примеру PASEMIB3C-164972", null=True, blank=True)

    def get_sub_tasks(self):
        from .MSubTask import SubTask

        sorted_SubTasks = list()
        not_sorted_SubTasks = SubTask.objects.filter(parent_task=self)
        sorted_SubTasks += not_sorted_SubTasks.filter(status=SubTask.IN_PROGRESS).order_by('priority').reverse()
        sorted_SubTasks += not_sorted_SubTasks.filter(status=SubTask.DIFFERED).order_by('priority').reverse()
        sorted_SubTasks += not_sorted_SubTasks.filter(status=SubTask.BLOCKED).order_by('priority').reverse()
        sorted_SubTasks += not_sorted_SubTasks.filter(status=SubTask.PENDING).order_by('priority').reverse()
        sorted_SubTasks += not_sorted_SubTasks.filter(status=SubTask.DONE).order_by('priority').reverse()
        sorted_SubTasks += not_sorted_SubTasks.filter(status=SubTask.DEPRECATED).order_by('priority').reverse()
        sorted_SubTasks += not_sorted_SubTasks.filter(status=SubTask.REJECTED).order_by('priority').reverse()
        return sorted_SubTasks

    def __str__(self):
        return "[" + self.group + "]: " + self.title


admin.site.register(Task)
