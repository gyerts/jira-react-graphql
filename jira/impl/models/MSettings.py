from django.db import models
from django.contrib import admin


class Settings(models.Model):
    name = models.CharField(max_length=255, help_text="имя сеттингов", default="no name")
    max_priority = models.DecimalField(decimal_places=1, max_digits=20, default=0, help_text="Максимальный приоритет таски в системе")

    def get_next_prio(self):
        self.max_priority += 1
        self.save()
        return self.max_priority

    def __str__(self):
        return self.name


admin.site.register(Settings)
