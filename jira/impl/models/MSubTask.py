from django.db import models
from datetime import datetime
from django.contrib import admin
import requests


class SubTask(models.Model):
    from .MTask import Task

    title = models.CharField(max_length=1000, null=True, blank=True)
    parent_task = models.ForeignKey(Task, on_delete=models.CASCADE)

    time_start = models.DateTimeField(null=True, blank=True)

    spent_minutes = models.IntegerField(default=0)
    logged_minutes = models.IntegerField(default=0)
    priority = models.IntegerField(default=0, help_text="Приоритет отображения, чем выше тем больше")

    IN_PROGRESS = 'IPR'
    DIFFERED = 'DIF'
    BLOCKED = 'BLC'
    PENDING = 'PND'
    DONE = 'DNE'
    DEPRECATED = 'DEP'
    REJECTED = 'REJ'

    STATUSES = (
        (PENDING, 'Pending'),
        (REJECTED, 'Rejected'),
        (DEPRECATED, 'Deprecated'),
        (DIFFERED, 'Differed'),
        (BLOCKED, 'Blocked'),
        (IN_PROGRESS, 'In progress'),
        (DONE, 'Done'),
    )
    status = models.CharField(
        max_length=3,
        choices=STATUSES,
        default=PENDING,
    )

    def get_statuses(self):
        return self.STATUSES

    def get_time_start(self):
        # year, month, date, hours, minutes, seconds
        retVal = None
        if self.time_start:
            retVal = ""
            retVal += str(self.time_start.year) + ", "
            retVal += str(self.time_start.month) + ", "
            retVal += str(self.time_start.day) + ", "
            retVal += str(self.time_start.hour) + ", "
            retVal += str(self.time_start.minute) + ", "
            retVal += str(self.time_start.second)
        return retVal

    def stop_timer(self):
        if self.time_start:
            task_start_time = datetime.strptime(self.time_start.ctime(), "%a %b %d %H:%M:%S %Y")
            delta = datetime.now() - task_start_time
            self.spent_minutes += int(delta.seconds / 60) + 1
            self.time_start = None
            self.save()

    def start_timer(self):
        self.time_start = datetime.now()
        self.status = self.IN_PROGRESS
        self.save()

    def log_work(self):
        if self.parent_task.log_task_id:
            url = "https://paiseu.luxoft.com/rest/api/2/issue/{log_task_id}/worklog".format(
                log_task_id=self.parent_task.log_task_id)

            # https://docs.atlassian.com/software/jira/docs/api/REST/7.6.1/#api/2/issue-addWorklog
            data = {
                "comment": self.title,
                # "started": "2018-06-04T12:44:00.000+0300",
                "timeSpentSeconds": self.spent_minutes * 60,
            }

            print("try log time: {url}, data={data}".format(url=url, data=data))

            headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
            r = requests.post(url, auth=("ygyerts", "performance_2019_10"), json=data, headers=headers)

            print(r.url, r.status_code)
            print(r.text)

            if r.status_code == 201:
                self.logged_minutes += self.spent_minutes
                self.spent_minutes = 0
                self.save()
            else:
                print("reject log time due to status code no equal 201 [status code = " + str(r.status_code) + "]")

    def change_status(self, new_status):
        self.status = new_status
        self.save()

    def __str__(self):
        return self.title


admin.site.register(SubTask)
