from graphene_django import DjangoObjectType
import graphene

from . import models
from . import types
from . import mutations

###############################################
###############################################
###############################################


class Query(graphene.ObjectType):
    all_tasks = graphene.List(types.TaskType)
    task = graphene.Field(types.TaskType, id=graphene.Int())
    custom_task = graphene.Field(types.CustomType, id=graphene.Int())

    def resolve_all_tasks(self, info):
        return models.Task.objects.all()

    def resolve_task(self, info, **kwargs):
        tid = kwargs.get('id')

        if tid is not None:
            return models.Task.objects.get(pk=tid)
        return None

    def resolve_custom_task(self, info, **kwargs):
        tid = kwargs.get('id')
        cq = types.CustomType()

        if tid is not None:
            task = models.Task.objects.get(pk=tid)
            cq.task = task
            cq.message = "Something special!"
            return cq
        return None


schema = graphene.Schema(query=Query, mutation=mutations.UpdateMutations)
